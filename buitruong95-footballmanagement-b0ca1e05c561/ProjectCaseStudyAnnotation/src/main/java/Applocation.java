import Helper.InsertHelper;
import Helper.SelectHelper;
import JavaClasses.*;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import java.util.Date;
import java.util.List;

public class Applocation {
    private static SessionFactory sessionFactory;
    private static InsertHelper insertHelper;
    private static SelectHelper selectHelper;

    public static void main(String[] args){
        Configuration configuration = new Configuration().configure();
        ServiceRegistryBuilder registry = new ServiceRegistryBuilder();
        registry.applySettings(configuration.getProperties());
        ServiceRegistry serviceRegistry = registry.buildServiceRegistry();

        // builds a session factory from the service registry
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        insertHelper = new InsertHelper(sessionFactory);
        selectHelper = new SelectHelper(sessionFactory);
        displayCustomer();
        //insert();
    }

    public static void displayCustomer(){
        List<Customer> list = selectHelper.getAllCustomer();
        for (int i=0; i<list.size(); i++){
            System.out.println("CustomerNumber         : " + list.get(i).getCustomerNumber());
            System.out.println("CustomerName           : " + list.get(i).getCustomerName());
            System.out.println("ContactLastName        : " + list.get(i).getContactLastName());
            System.out.println("ContactFristName       : " + list.get(i).getContactFirstName());
            System.out.println("Phone                  : " + list.get(i).getPhone());
            System.out.println("AddressLine1           : " + list.get(i).getAddressLine1());
            System.out.println("AddressLine2           : " + list.get(i).getAddressLine2());
            System.out.println("City                   : " + list.get(i).getCity());
            System.out.println("State                  : " + list.get(i).getState());
            System.out.println("PostalCode             : " + list.get(i).getPostalCode());
            System.out.println("Country                : " + list.get(i).getCountry());
            System.out.println("CreditLimit            : " + list.get(i).getCreditLimit());
            System.out.println("SalesRepEmployeeNumber : " + list.get(i).getEmployee().getEmployeeNumber());
            System.out.println("------------------------");
        }
    }

    public static void insert(){
        ProductLine productLine = new ProductLine();
        productLine.setProductLine("Thien Long");
        productLine.setTextDescription("San Pham Thien Long");
        productLine.setHtmlDescription("linkthienlong");
        productLine.setImage("imagethienlong");

        Product product = new Product();
        product.setProductCode("T50");
        product.setProductName("Sach vo");
        product.setProductScale("high");
        product.setProductVendor("Supermarket");
        product.setProductDescription("mau sang");
        product.setQuantityInStock(1000);
        product.setBuyPrice(500);
        product.setMSRP(0.2);
        product.setProductLine(productLine);

        Product product1 = new Product();
        product1.setProductCode("K90");
        product1.setProductName("But Chi");
        product1.setProductScale("high");
        product1.setProductVendor("Supermarket");
        product1.setProductDescription("Ngoi Kim");
        product1.setQuantityInStock(5000);
        product1.setBuyPrice(1000);
        product1.setMSRP(0.5);
        product1.setProductLine(productLine);

        Office office = new Office();
        office.setOfficeCode("THTH");
        office.setCity("Hai Phong");
        office.setPhone("0976");
        office.setAddressLine1("Do Son");
        office.setAddressLine2("Sam Son");
        office.setState("Dat Cang");
        office.setCountry("Viet Nam");
        office.setPostalCode("9876");
        office.setTerritory("jhhj");

        Employee e = new Employee();
        e.setEmployeeNumber(444);
        e.setLastName("Nga");
        e.setFirstName("Duong");
        e.setExtension("Management");
        e.setEmail("ngaduong95@gmail.com");
        e.setJobTitle("Director");
        e.setOffice(office);

        Employee e1 = new Employee();
        e1.setEmployeeNumber(555);
        e1.setLastName("Tuan");
        e1.setFirstName("Pham");
        e1.setExtension("trainer");
        e1.setEmail("tuanpham95@gmail.com");
        e1.setJobTitle("employee");
        e1.setOffice(office);
        e1.setEmployee(e);

        Employee e2 = new Employee();
        e2.setEmployeeNumber(666);
        e2.setLastName("Dinh");
        e2.setFirstName("Tran");
        e2.setExtension("work");
        e2.setEmail("dinhtran95@gmail.com");
        e2.setJobTitle("employee");
        e2.setOffice(office);
        e2.setEmployee(e);

        Customer customer = new Customer();
        customer.setCustomerNumber(30);
        customer.setCustomerName("Messi");
        customer.setContactLastName("Messi");
        customer.setContactFirstName("Leone");
        customer.setPhone("0999");
        customer.setAddressLine1("Barcelona");
        customer.setAddressLine2("Camp Nou");
        customer.setCity("Espanol");
        customer.setState("Province");
        customer.setPostalCode("9889");
        customer.setCountry("Spain");
        customer.setCreditLimit(666);
        customer.setEmployee(e1);

        Order order = new Order();
        order.setOrderNumber(3);
        Date date = new Date();
        order.setOrderDate(date);
        order.setRequiredDate(date);
        order.setShippedDate(date);
        order.setStatus("processing");
        order.setComments("very good");
        order.setCustomer(customer);

        OrderDetail OD = new OrderDetail();
        OD.setQuantityOrdered(100);
        OD.setId(new OrderDetailId(3,"T50"));
        OD.setPriceEach(6000);
        OD.setOrderLineNumber(6);
        OD.setProduct(product);
        OD.setOrder(order);

        OrderDetail OD1 = new OrderDetail();
        OD1.setQuantityOrdered(50);
        OD1.setId(new OrderDetailId(3,"K90"));
        OD1.setPriceEach(7000);
        OD1.setOrderLineNumber(7);
        OD1.setProduct(product);
        OD1.setOrder(order);

        Payment payment = new Payment();
        payment.setCheckNumber("8888");
        payment.setId(new PaymentId(30,date));
        payment.setAmount(5.5);
        payment.setCustomer(customer);

        insertHelper.insertDB(productLine);
        insertHelper.insertDB(product);
        insertHelper.insertDB(product1);
        insertHelper.insertDB(office);
        insertHelper.insertDB(e);
        insertHelper.insertDB(e1);
        insertHelper.insertDB(e2);
        insertHelper.insertDB(customer);
        insertHelper.insertDB(order);
        insertHelper.insertDB(OD);
        insertHelper.insertDB(OD1);
        insertHelper.insertDB(payment);
    }
}
