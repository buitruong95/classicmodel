package JavaClasses;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Date;

@Embeddable
public class PaymentId implements Serializable {
    @Column(name = "customernumber")
    private int customerNumber;

    @Column(name = "paymentdate")
    private Date paymentDate;

    public PaymentId() {
    }

    public PaymentId(int customerNumber, Date paymentDate) {
        this.customerNumber = customerNumber;
        this.paymentDate = paymentDate;
    }

    public int getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(int customerNumber) {
        this.customerNumber = customerNumber;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (!this.getClass().equals(o.getClass())) return false;
        PaymentId paymentId = (PaymentId) o;
        if((this.customerNumber == paymentId.getCustomerNumber()) && this.paymentDate.equals(paymentId.getPaymentDate())){
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return customerNumber + getPaymentDate().hashCode();
    }
}
