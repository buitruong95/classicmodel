package JavaClasses;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "orderdetails")
public class OrderDetail extends Father implements Serializable{
    @EmbeddedId
    private OrderDetailId id;

    @Column(name = "quantityordered")
    private int quantityOrdered;

    @Column(name = "priceeach")
    private double priceEach;

    @Column(name = "orderlinenumber")
    private int orderLineNumber;

    @MapsId("ordernumber")
    @ManyToOne
    @JoinColumn(name = "ordernumber")
    private Order order;

    @MapsId("productcode")
    @ManyToOne
    @JoinColumn(name = "productcode")
    private Product product;

    public OrderDetailId getId() {
        return id;
    }

    public void setId(OrderDetailId id) {
        this.id = id;
    }

    public int getQuantityOrdered() {
        return quantityOrdered;
    }

    public void setQuantityOrdered(int quantityOrdered) {
        this.quantityOrdered = quantityOrdered;
    }

    public double getPriceEach() {
        return priceEach;
    }

    public void setPriceEach(double priceEach) {
        this.priceEach = priceEach;
    }

    public int getOrderLineNumber() {
        return orderLineNumber;
    }

    public void setOrderLineNumber(int orderLineNumber) {
        this.orderLineNumber = orderLineNumber;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
