package Helper;

import JavaClasses.Product;
import JavaClasses.ProductLine;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class DeleteHelper {
    private SessionFactory factory;

    public DeleteHelper(SessionFactory factory){
        this.factory=factory;
    }

    public void deleteProduct(String productCode){
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        try{
            Product product= (Product) session.get(Product.class, productCode);
            session.delete(product);
            transaction.commit();
        }catch (HibernateException e){
            if(transaction!=null) transaction.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }

    public void deleteProductLine(String productline){
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        try {
            ProductLine productLine= (ProductLine) session.get(ProductLine.class, productline);
            session.delete(productLine);
            transaction.commit();
        }catch (HibernateException e){
            if(transaction!=null) transaction.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }
}