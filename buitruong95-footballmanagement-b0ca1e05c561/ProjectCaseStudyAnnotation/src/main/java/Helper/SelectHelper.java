package Helper;

import JavaClasses.Customer;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class SelectHelper {
    private SessionFactory factory;

    public SelectHelper(SessionFactory factory) {
        this.factory = factory;
    }

    public List<Customer> getAllCustomer(){
        Session session=factory.openSession();
        Transaction transaction= session.beginTransaction();
        List<Customer> customerList = new ArrayList<Customer>();

        try{
            customerList = (List<Customer>) session.createQuery("FROM Customer").list();
            transaction.commit();
        }catch (HibernateException e){
            e.printStackTrace();
        }finally {
            session.close();
        }
        return customerList;
    }
}
