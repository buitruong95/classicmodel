package JavaClasses;

import java.io.Serializable;
import java.util.Date;

public class Payment extends Father implements Serializable{
    private int customerNumber;
    private Date paymentDate;
    private String checkNumber;
    private double amount;
    private Customer customer;

    public int getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(int customerNumber) {
        this.customerNumber = customerNumber;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getCheckNumber() {
        return checkNumber;
    }

    public void setCheckNumber(String checkNumber) {
        this.checkNumber = checkNumber;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public boolean equals(Object obj){
        if(obj==null) return false;
        if(!this.getClass().equals(obj.getClass())) return false;
        Payment payment = (Payment) obj;
        if((this.customerNumber == payment.getCustomerNumber()) && (this.paymentDate.equals(payment.getPaymentDate()))){
            return true;
        }return false;
    }

//    public int hashCode(){
//        int tmp = 0;
//        tmp = (customerNumber).hashCode();
//        return tmp;
//    }
}
