import Helper.DeleteHelper;
import Helper.InsertHelper;
import Helper.SelectHelper;
import JavaClasses.*;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import java.util.Date;
import java.util.List;

public class Application {
    private static SessionFactory sessionFactory;
    private static InsertHelper insertHelper;
    private static SelectHelper selectHelper;
    public static void main(String[] args){
        Configuration configuration = new Configuration().configure();
        ServiceRegistryBuilder registry = new ServiceRegistryBuilder();
        registry.applySettings(configuration.getProperties());
        ServiceRegistry serviceRegistry = registry.buildServiceRegistry();

        // builds a session factory from the service registry
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);

        insertHelper = new InsertHelper(sessionFactory);
        selectHelper = new SelectHelper(sessionFactory);
        displayCustomer();
        //insert();

    }

    public static void displayCustomer(){
        List<Customer> list = selectHelper.getAllCustomer();
        for (int i=0; i<list.size(); i++){
            System.out.println("CustomerNumber         : " + list.get(i).getCustomerNumber());
            System.out.println("CustomerName           : " + list.get(i).getCustomerName());
            System.out.println("ContactLastName        : " + list.get(i).getContactLastName());
            System.out.println("ContactFristName       : " + list.get(i).getContactFirstName());
            System.out.println("Phone                  : " + list.get(i).getPhone());
            System.out.println("AddressLine1           : " + list.get(i).getAddressLine1());
            System.out.println("AddressLine2           : " + list.get(i).getAddressLine2());
            System.out.println("City                   : " + list.get(i).getCity());
            System.out.println("State                  : " + list.get(i).getState());
            System.out.println("PostalCode             : " + list.get(i).getPostalCode());
            System.out.println("Country                : " + list.get(i).getCountry());
            System.out.println("CreditLimit            : " + list.get(i).getCreditLimit());
            System.out.println("SalesRepEmployeeNumber : " + list.get(i).getEmployee().getEmployeeNumber());
            System.out.println("------------------------");
        }
    }

    public static void insert(){
        ProductLine productLine = new ProductLine();
        productLine.setProductLine("Long Hai");
        productLine.setTextDescription("San Pham Long hai");
        productLine.setHtmlDescription("linklonghai");
        productLine.setImage("imagelonghai");

        Product product = new Product();
        product.setProductCode("MP3");
        product.setProductName("Thach Rau Cau");
        product.setProductScale("Good");
        product.setProductVendor("Supermarket");
        product.setProductDescription("ngon bo");
        product.setQuantityInStock(5000);
        product.setBuyPrice(1000);
        product.setMSRP(0.4);
        product.setProductLine(productLine);

        Product product1 = new Product();
        product1.setProductCode("MP4");
        product1.setProductName("Nuoc Uong");
        product1.setProductScale("high");
        product1.setProductVendor("Supermarket");
        product1.setProductDescription("Khong Duong");
        product1.setQuantityInStock(3000);
        product1.setBuyPrice(2000);
        product1.setMSRP(0.4);
        product1.setProductLine(productLine);

        Office office = new Office();
        office.setOfficeCode("MPMP");
        office.setCity("Ho Chi Minh");
        office.setPhone("0163");
        office.setAddressLine1("Quan 1");
        office.setAddressLine2("Quan 2");
        office.setState("Sai Gon");
        office.setCountry("Viet Nam");
        office.setPostalCode("8567");
        office.setTerritory("hgnf");

        Employee e = new Employee();
        e.setEmployeeNumber(3306);
        e.setLastName("Hong");
        e.setFirstName("Tran");
        e.setExtension("Management");
        e.setEmail("hongtran95@gmail.com");
        e.setJobTitle("Director");
        e.setOffice(office);
        e.setEmployee(e);

        Employee e1 = new Employee();
        e1.setEmployeeNumber(1532);
        e1.setLastName("Huyen");
        e1.setFirstName("Minh");
        e1.setExtension("trainer");
        e1.setEmail("huyenminh95@gmail.com");
        e1.setJobTitle("employee");
        e1.setOffice(office);
        e1.setEmployee(e);

        Employee e2 = new Employee();
        e2.setEmployeeNumber(666);
        e2.setLastName("Mai");
        e2.setFirstName("Tran");
        e2.setExtension("work");
        e2.setEmail("maitran95@gmail.com");
        e2.setJobTitle("employee");
        e2.setOffice(office);
        e2.setEmployee(e);

        Customer customer = new Customer();
        customer.setCustomerNumber(40);
        customer.setCustomerName("Suarez");
        customer.setContactLastName("Suarez");
        customer.setContactFirstName("Luis");
        customer.setPhone("0915");
        customer.setAddressLine1("Barcelona");
        customer.setAddressLine2("Camp Nou");
        customer.setCity("Espanol");
        customer.setState("Province");
        customer.setPostalCode("9010");
        customer.setCountry("Spain");
        customer.setCreditLimit(130);
        customer.setEmployee(e1);

        Order order = new Order();
        order.setOrderNumber(4);
        Date date = new Date();
        order.setOrderDate(date);
        order.setRequiredDate(date);
        order.setShippedDate(date);
        order.setStatus("processing");
        order.setComments("satisfy");
        order.setCustomer(customer);

        OrderDetail OD = new OrderDetail();
        OD.setQuantityOrdered(100);
        OD.setOrderNumber(4);
        OD.setProductCode("MP3");
        OD.setPriceEach(6000);
        OD.setOrderLineNumber(6);
        OD.setProduct(product);
        OD.setOrder(order);

        OrderDetail OD1 = new OrderDetail();
        OD1.setQuantityOrdered(50);
        OD1.setOrderNumber(4);
        OD1.setProductCode("MP4");
        OD1.setPriceEach(7000);
        OD1.setOrderLineNumber(7);
        OD1.setProduct(product1);
        OD1.setOrder(order);

        Payment payment = new Payment();
        payment.setCheckNumber("8888");
        payment.setCustomerNumber(40);
        payment.setPaymentDate(date);
        payment.setAmount(5.5);
        payment.setCustomer(customer);

        insertHelper.insertDB(productLine);
        insertHelper.insertDB(product);
        insertHelper.insertDB(product1);
        insertHelper.insertDB(office);
        insertHelper.insertDB(e);
        insertHelper.insertDB(e1);
        insertHelper.insertDB(e2);
        insertHelper.insertDB(customer);
        insertHelper.insertDB(order);
        insertHelper.insertDB(OD);
        insertHelper.insertDB(OD1);
        insertHelper.insertDB(payment);
    }
}
