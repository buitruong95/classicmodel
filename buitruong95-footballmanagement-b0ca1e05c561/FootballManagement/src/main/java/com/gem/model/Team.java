package com.gem.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "team")
public class Team implements Serializable {
    @Id
    @Column(name = "code")
    private String code;

    @Column(name = "name")
    private String nameTeam;

    @OneToOne
    @JoinColumn(name = "codestadium")
    private Stadium stadium;

    @Column(name = "numberplayer")
    private int numberPlayer;

    @Column(name = "numberplayedmatch")
    private int numberPlayedMatch;

    @Column(name = "points")
    private int points;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNameTeam() {
        return nameTeam;
    }

    public void setNameTeam(String nameTeam) {
        this.nameTeam = nameTeam;
    }

    public Stadium getStadium() {
        return stadium;
    }

    public void setStadium(Stadium stadium) {
        this.stadium = stadium;
    }

    public int getNumberPlayer() {
        return numberPlayer;
    }

    public void setNumberPlayer(int numberPlayer) {
        this.numberPlayer = numberPlayer;
    }

    public int getNumberPlayedMatch() {
        return numberPlayedMatch;
    }

    public void setNumberPlayedMatch(int numberPlayedMatch) {
        this.numberPlayedMatch = numberPlayedMatch;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
