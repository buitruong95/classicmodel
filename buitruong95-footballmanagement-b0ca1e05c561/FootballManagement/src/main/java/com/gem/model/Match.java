package com.gem.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "match")
public class Match implements Serializable {
    @Id
    @Column(name = "codematch")
    private String code;

    @Column(name = "codeteam1")
    private String codeTeam1;

    @Column(name = "codeteam2")
    private String codeTeam2;

    @Column(name = "time")
    private String time;

    @OneToOne
    @JoinColumn(name = "codestadium")
    private Stadium stadium;

    @Column(name = "fixture")
    private int fixture;

    @Column(name = "result")
    private String result;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeTeam1() {
        return codeTeam1;
    }

    public void setCodeTeam1(String codeTeam1) {
        this.codeTeam1 = codeTeam1;
    }

    public String getCodeTeam2() {
        return codeTeam2;
    }

    public void setCodeTeam2(String codeTeam2) {
        this.codeTeam2 = codeTeam2;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Stadium getStadium() {
        return stadium;
    }

    public void setStadium(Stadium stadium) {
        this.stadium = stadium;
    }

    public int getFixture() {
        return fixture;
    }

    public void setFixture(int fixture) {
        this.fixture = fixture;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
