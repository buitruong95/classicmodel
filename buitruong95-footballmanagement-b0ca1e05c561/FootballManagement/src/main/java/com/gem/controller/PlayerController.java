package com.gem.controller;

import com.gem.model.Player;
import com.gem.model.Team;
import com.gem.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/player")
public class PlayerController {

    public PlayerController() {
        System.out.println("PlayerController()");
    }

    @Autowired
    private PlayerService playerService;

    @GetMapping("/list")
    public String list(Model model){
        List<Player> listPlayer = playerService.getAllPlayer();
        model.addAttribute("title", "Danh sách cầu thủ.");
        model.addAttribute("players", listPlayer);
        return "player/list";
    }

    @GetMapping("/listPlayersByTeam/{code}")
    public String listPlayerByTeam(@PathVariable String code, Model model){
        List<Player> listPlayer = playerService.getPlayersByTeam(code);
        model.addAttribute("Title", "List Player by Team ");
        model.addAttribute("players",listPlayer);
        return "player/list";
    }

    @GetMapping("/edit/{code}")
    public String edit(@PathVariable String code, Model model){
        Player player = playerService.searchByCode(code);

        model.addAttribute("title", "Chỉnh sửa player");
        model.addAttribute("player", player);
        return "player/form";
    }

    @GetMapping("/create")
    public String create(Model model){
        Player player = new Player();

        model.addAttribute("title", "Thêm mới player");
        model.addAttribute("player", player);
        return "player/form";
    }

    @PostMapping("/save")
    public String save(@Valid Player player, Team team, BindingResult result, RedirectAttributes redirect){
        if(result.hasErrors()){
            System.out.println("Error");

            for (ObjectError error : result.getAllErrors()){
                System.out.println(error);
            }
            return "player/form";
        }
        playerService.addPlayerToTeam(team,player.getCodePlayer());
        redirect.addFlashAttribute("message", "Add thanh cong!");
        return "redirect:/player/list";
    }

    @GetMapping("/delete/{code}")
    public String delete(@PathVariable String code, RedirectAttributes redirect){
        playerService.removePlayerFromTheTeam(code);
        redirect.addFlashAttribute("message", "Xoa thanh cong!");
        return "redirect:/player/list";
    }
}
