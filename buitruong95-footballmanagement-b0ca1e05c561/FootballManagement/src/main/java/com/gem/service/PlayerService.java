package com.gem.service;

import com.gem.model.Player;
import com.gem.model.Team;

import java.util.List;

public interface PlayerService {
    public Player searchByCode(String code);

    public void addPlayerToTeam(Team team, String code);

    public void removePlayerFromTheTeam(String code);

    public List<Player> getPlayersByTeam(String code);

    public List<Player> getAllPlayer();
}
