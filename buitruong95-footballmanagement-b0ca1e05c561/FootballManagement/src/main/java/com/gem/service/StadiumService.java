package com.gem.service;

import com.gem.model.Stadium;

import java.util.List;

public interface StadiumService {
    Stadium FindStadium(String code);
    void UpdateStadium(Stadium stadium);
    List<Stadium> GetAllStadium();
}
