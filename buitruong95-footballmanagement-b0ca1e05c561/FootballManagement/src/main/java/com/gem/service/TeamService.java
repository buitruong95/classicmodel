package com.gem.service;

import com.gem.model.Team;

import java.util.List;

public interface TeamService {
    Team FindTeam(String code);
    void UpdateTeam(Team team);
    void AddTeam(Team team);
    List<Team> GetAllTeam();
}
