package com.gem.service;

import com.gem.model.Match;

import java.util.List;

public interface MatchService {
    Match FindMatch(String code);
    void UpdateMatch(Match match);
    List<Match> GetAllMatch();
}
