package com.gem.dao;

import com.gem.model.Team;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TeamDaoImpl implements TeamDao {
    @Autowired
    private SessionFactory sessionFactory;

    public Team FindTeam(String code){
        return (Team) sessionFactory.getCurrentSession().get(Team.class,code);
    }

    public void AddTeam(Team team){
        sessionFactory.getCurrentSession().saveOrUpdate(team);
    }

    public void UpdateTeam(Team team){
        sessionFactory.getCurrentSession().saveOrUpdate(team);
    }

    public List<Team> GetAllTeam(){
        return sessionFactory.getCurrentSession().createQuery("FROM Team").list();
    }
}
