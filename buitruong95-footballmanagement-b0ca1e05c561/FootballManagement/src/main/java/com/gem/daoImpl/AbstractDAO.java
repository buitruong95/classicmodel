package com.gem.dao;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

public abstract class AbstractDAO<T extends Serializable> {
    private final Class<T> persistentClass;
    @SuppressWarnings("unchecked")
    public AbstractDAO(){
        this.persistentClass = (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    @Autowired
    private SessionFactory sessionFactory;

    protected Session getSession(){
        return sessionFactory.getCurrentSession();
    }

    public T findBycode(String code){
        return (T) getSession().get(persistentClass, code);
    }

    public void persist(T entity){
        getSession().persist(entity);
    }

    public void delete(T entity){
        getSession().delete(entity);
    }

    public void insert(T entity){
        getSession().save(entity);
    }

    public void deleteByCode(String code){
        T t = (T) getSession().get(persistentClass, code);
        getSession().delete(t);
    }

    public List<T> findAll(){
        return createEntityCriteria().list();
    }

    protected Criteria createEntityCriteria(){
        return getSession().createCriteria(persistentClass);
    }

}
