package com.gem.dao;

import com.gem.model.Stadium;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StadiumDaoImpl implements StadiumDao {
    @Autowired
    private SessionFactory sessionFactory;

    public Stadium FindStadium(String code){
        return (Stadium) sessionFactory.getCurrentSession().get(Stadium.class,code);
    }

    public void UpdateStadium(Stadium stadium){
        sessionFactory.getCurrentSession().saveOrUpdate(stadium);
    }

    public List<Stadium> GetAllStadium(){
        return sessionFactory.getCurrentSession().createQuery("FROM Stadium").list();
    }
}
