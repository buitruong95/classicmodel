package com.gem.dao;

import com.gem.model.Match;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MatchDaoImpl implements MatchDao {
    @Autowired
    private SessionFactory sessionFactory;

    public Match FindMatch(String code){
        return (Match) sessionFactory.getCurrentSession().get(Match.class,code);
    }

    public void UpdateMatch(Match match){
        sessionFactory.getCurrentSession().saveOrUpdate(match);
    }

    public List<Match> GetAllMatch(){
        return sessionFactory.getCurrentSession().createQuery("FROM Match").list();
    }
}
