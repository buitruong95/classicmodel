package com.gem.dao;

import com.gem.model.Player;
import com.gem.model.Team;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("playerDao")
public class PlayerDaoImpl extends AbstractDAO<Player> implements PlayerDao {

    @Override
    public List<Player> getPlayersByTeam(String code) {
        Query query = getSession().createQuery("from " + Player.class.getName()
                + " where team.getCode() = " + code);
        return (List<Player>) query.list();
    }

    @Override
    public void addPlayerToTeam(Team team, String code) {
        Player player = findBycode(code);
        player.setTeam(team);
        persist(player);
    }

    @Override
    public void removePlayerFromTheTeam(String code) {
        Player player = findBycode(code);
        delete(player);
    }

    @Override
    public Player searchByCode(String code) {
        Player player = findBycode(code);
        return player;
    }

    @Override
    public List<Player> getAllPlayer() {
        Query query = getSession().createQuery("from " + Player.class.getName());
        return (List<Player>) query.list();
    }

}
