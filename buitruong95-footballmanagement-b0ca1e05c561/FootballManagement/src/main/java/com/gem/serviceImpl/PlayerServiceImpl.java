package com.gem.service;

import com.gem.dao.AbstractDAO;
import com.gem.dao.PlayerDao;
import com.gem.dao.TeamDao;
import com.gem.model.Player;
import com.gem.model.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PlayerServiceImpl implements PlayerService {
    @Autowired
    private PlayerDao playerDao;

    @Override
    @Transactional
    public void addPlayerToTeam(Team team, String code){
        playerDao.addPlayerToTeam(team,code);
    }

    @Override
    @Transactional
    public void removePlayerFromTheTeam(String code){
        playerDao.removePlayerFromTheTeam(code);
    }

    @Override
    @Transactional
    public List<Player> getPlayersByTeam(String code){
        return playerDao.getPlayersByTeam(code);
    }

    @Override
    @Transactional
    public List<Player> getAllPlayer(){
        return playerDao.getAllPlayer();
    }

    @Override
    @Transactional
    public Player searchByCode(String code){
        return playerDao.searchByCode(code);
    }

    public void setPlayerDao(PlayerDao playerDao){
        this.playerDao = playerDao;
    }
}
