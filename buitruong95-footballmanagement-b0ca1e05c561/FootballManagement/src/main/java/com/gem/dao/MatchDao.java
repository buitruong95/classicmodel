package com.gem.dao;

import com.gem.model.Match;

import java.util.List;

public interface MatchDao {
    Match FindMatch(String code);
    void UpdateMatch(Match match);
    List<Match> GetAllMatch();
}
