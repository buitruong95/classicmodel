package com.gem.dao;

import com.gem.model.Player;
import com.gem.model.Team;

import java.util.List;

public interface PlayerDao {
    Player searchByCode(String code);

    void addPlayerToTeam(Team team, String code);

    void removePlayerFromTheTeam(String code);

    List<Player> getPlayersByTeam(String code);

    List<Player> getAllPlayer();
}
