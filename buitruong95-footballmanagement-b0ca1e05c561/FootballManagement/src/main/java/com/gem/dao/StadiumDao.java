package com.gem.dao;

import com.gem.model.Stadium;

import java.util.List;

public interface StadiumDao {
    Stadium FindStadium(String code);
    void UpdateStadium(Stadium stadium);
    List<Stadium> GetAllStadium();
}
