package com.gem.dao;

import com.gem.model.Team;

import java.util.List;

public interface TeamDao {
    Team FindTeam(String code);
    void UpdateTeam(Team team);
    void AddTeam(Team team);
    List<Team> GetAllTeam();
}
