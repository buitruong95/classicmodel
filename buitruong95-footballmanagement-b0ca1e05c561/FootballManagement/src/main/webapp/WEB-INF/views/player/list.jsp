<%@ page language="java" contentType="text/html; ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">
    <title>Football Management</title>

    <style>
        tr:first-child{
            font-weight: bold;
            background-color: #C6C9C4;
        }
    </style>
</head>
<body>
    <h2>List of Player</h2>
    <table>
        <tr>
            <td>Code</td><td>Name</td><td>CodeTeam</td><td>DateOfBirth</td><td>Country</td><td>Position</td>
            <c:forEach items="${players}" var="player">
                <tr>
                    <td>${player.codePlayer}</td>
                    <td>${player.namePlayer}</td>
                    <td>${player.team.code}</td>
                    <td>${player.dateOfBirth}</td>
                    <td>${player.country}</td>
                    <td>${player.position}</td>
                </tr>
            </c:forEach>
        </tr>
    </table>
</body>
</html>