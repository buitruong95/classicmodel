package JavaClasses;

import java.io.Serializable;

public class OrderDetail extends Father implements Serializable{
    private int orderNumber;
    private String productCode;
    private int quantityOrdered;
    private double priceEach;
    private int orderLineNumber;
    private Product product;
    private Order order;

    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    public int getQuantityOrdered() {
        return quantityOrdered;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public void setQuantityOrdered(int quantityOrdered) {
        this.quantityOrdered = quantityOrdered;
    }

    public double getPriceEach() {
        return priceEach;
    }

    public void setPriceEach(double priceEach) {
        this.priceEach = priceEach;
    }

    public int getOrderLineNumber() {
        return orderLineNumber;
    }

    public void setOrderLineNumber(int orderLineNumber) {
        this.orderLineNumber = orderLineNumber;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public int hashCode(){
        int tmp=0;
        tmp = (orderNumber + productCode).hashCode();
        return tmp;
    }

    public boolean equals(Object obj){
        if (obj == null) return false;
        if (!this.getClass().equals(obj.getClass())) return false;

        OrderDetail obj2 = (OrderDetail)obj;
        if((this.orderNumber == obj2.getOrderNumber()) && (this.productCode.equals(obj2.getProductCode())))
        {
            return true;
        }
        return false;
    }
}
