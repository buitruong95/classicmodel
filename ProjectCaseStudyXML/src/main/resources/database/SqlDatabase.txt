1. sql's command create table productlines
	CREATE TABLE public.productlines
	(
    "productLine(50)" VARCHAR(50) DEFAULT NULL PRIMARY KEY NOT NULL,
    textDescription VARCHAR(4000) DEFAULT NULL,
    htmlDescription TEXT DEFAULT NULL,
    image VARCHAR(2048) DEFAULT NULL
	);
2. sql's command create table products
	CREATE TABLE public.products
	(
    productCode VARCHAR(15) DEFAULT NULL  PRIMARY KEY NOT NULL,
    productName VARCHAR(70) DEFAULT NULL  NOT NULL,
    productLine VARCHAR(50) DEFAULT NULL  NOT NULL,
    productScale VARCHAR(10) DEFAULT NULL  NOT NULL,
    productVendor VARCHAR(50) DEFAULT NULL  NOT NULL,
    productDescription TEXT DEFAULT NULL  NOT NULL,
    quantityInStock SMALLINT DEFAULT NULL  NOT NULL,
    buyPrice DOUBLE PRECISION DEFAULT NULL  NOT NULL,
    MSRP DOUBLE PRECISION DEFAULT NULL  NOT NULL,
    CONSTRAINT "products_productlines_productLine(50)_fk" FOREIGN KEY (productLine) REFERENCES productlines ("productline")
	);
3. sql's command create table offices
	CREATE TABLE public.offices
	(
    officeCode VARCHAR(10) DEFAULT NULL  PRIMARY KEY NOT NULL,
    city VARCHAR(50) DEFAULT NULL  NOT NULL,
    phone VARCHAR(50) DEFAULT NULL  NOT NULL,
    addressLine1 VARCHAR(50) DEFAULT NULL  NOT NULL,
    addressLine2 VARCHAR(50) DEFAULT NULL ,
    state VARCHAR(50) DEFAULT NULL ,
    country VARCHAR(50) DEFAULT NULL  NOT NULL,
    postalCode VARCHAR(15) DEFAULT NULL  NOT NULL,
    territory VARCHAR(10) DEFAULT NULL  NOT NULL
	);
4. 	sql's command create table employees
	CREATE TABLE public.employees
(
    employeeNumber INT DEFAULT NULL  PRIMARY KEY NOT NULL,
    lastName VARCHAR(50) DEFAULT NULL  NOT NULL,
    firstName VARCHAR(50) DEFAULT NULL  NOT NULL,
    extension VARCHAR(10) DEFAULT NULL  NOT NULL,
    email VARCHAR(100) DEFAULT NULL  NOT NULL,
    officeCode VARCHAR(10) DEFAULT NULL  NOT NULL,
    reportsTo INT DEFAULT NULL ,
    jobTitle VARCHAR(50) DEFAULT NULL  NOT NULL,
    CONSTRAINT employees_employees_reportsTo_fk FOREIGN KEY (reportsTo) REFERENCES employees (employeeNumber),
    CONSTRAINT employees_offices_officecode_fk FOREIGN KEY (officeCode) REFERENCES offices (officecode)
);
5. sql's command create table customers
	CREATE TABLE public.customers
(
    customerNumber INT DEFAULT NULL  PRIMARY KEY NOT NULL,
    customerName VARCHAR(50) DEFAULT NULL  NOT NULL,
    contactLastName VARCHAR(50) DEFAULT NULL  NOT NULL,
    contactFirstName VARCHAR(50) DEFAULT NULL  NOT NULL,
    phone VARCHAR(50) DEFAULT NULL  NOT NULL,
    addressLine1 VARCHAR(50) DEFAULT NULL  NOT NULL,
    addressLine2 VARCHAR(50) DEFAULT NULL ,
    city VARCHAR(50) DEFAULT NULL  NOT NULL,
    state VARCHAR(50) DEFAULT NULL ,
    postalCode VARCHAR(15) DEFAULT NULL ,
    country VARCHAR(50) DEFAULT NULL  NOT NULL,
    salesRepEmployeeNumber INT DEFAULT NULL ,
    creditLimit DOUBLE PRECISION DEFAULT NULL ,
    CONSTRAINT customers_employees_employeenumber_fk FOREIGN KEY (salesRepEmployeeNumber) REFERENCES employees (employeenumber)
);
6. sql's command create table orders
	CREATE TABLE public.orders
(
    orderNumber INT DEFAULT NULL  PRIMARY KEY NOT NULL,
    orderDate DATE DEFAULT NULL  NOT NULL,
    requiredDate DATE DEFAULT NULL  NOT NULL,
    shippedDate DATE DEFAULT NULL ,
    status VARCHAR(15) DEFAULT NULL  NOT NULL,
    comments TEXT DEFAULT NULL ,
    customerNumber INT DEFAULT NULL  NOT NULL,
    CONSTRAINT orders_customers_customernumber_fk FOREIGN KEY (customerNumber) REFERENCES customers (customernumber)
);
7. sql's command create table orderdetails
	CREATE TABLE public.orderdetails
	(
    orderNumber INT DEFAULT NULL  NOT NULL,
    productCode VARCHAR(15) DEFAULT NULL  NOT NULL,
    quantityOrdered INT DEFAULT NULL  NOT NULL,
    priceEach DOUBLE PRECISION DEFAULT NULL  NOT NULL,
    orderLineNumber INT DEFAULT NULL  NOT NULL,
    PRIMARY KEY (orderNumber,productCode),
    CONSTRAINT orderdetails_products_productcode_fk FOREIGN KEY (productCode) REFERENCES products (productcode),
    CONSTRAINT orderdetails_orders_ordernumber_fk FOREIGN KEY (orderNumber) REFERENCES orders (ordernumber)
	);
8.	sql's command create table payments
 	CREATE TABLE public.payments
	(
  	customerNumber INT DEFAULT NULL  NOT NULL,
 	paymentDate DATE DEFAULT NULL  NOT NULL,
  	checkNumber VARCHAR(50) DEFAULT NULL  NOT NULL,
  	amount DOUBLE PRECISION DEFAULT NULL  NOT NULL,
  	PRIMARY KEY (customerNumber,paymentDate),
  	CONSTRAINT payments_customers_customernumber_fk FOREIGN KEY (customerNumber) REFERENCES customers (customernumber)
	);